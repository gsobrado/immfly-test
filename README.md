# Immfly Test

This project is composed of a java project with SpringBoot, a json-web-server of node.js which acts as an external webservice and a redis database.

To start the project, clone the repository and run 

```
docker-compose build 
```

and then run 

```
docker-compose up
```

On port 8080 the webservice with the endpoints

to log in, call

http://localhost:8080/immfly/login

with the following json

{"username": "admin","password": "admin"}


after logging, with the token call

http://localhost:8080/immfly/filterFligths/EC-MYT/653
