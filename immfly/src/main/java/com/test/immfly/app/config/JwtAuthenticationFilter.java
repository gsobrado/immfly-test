package com.test.immfly.app.config;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
@Order(1)
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	
	@Value("${jwt.key}")
	String  signingKey;

	private boolean checkToken(HttpServletRequest request, HttpServletResponse res) {
		String authenticationHeader = request.getHeader(JwtTokenUtils.HEADER_STRING);
		if (authenticationHeader == null || !authenticationHeader.startsWith(JwtTokenUtils.TOKEN_PREFIX))
			return false;
		
		return true;
	}
	
	private Claims validateToken(HttpServletRequest request) {
		String jwtToken = request.getHeader(JwtTokenUtils.HEADER_STRING).replace(JwtTokenUtils.TOKEN_PREFIX, "");

		return Jwts.parser().setSigningKey(signingKey.getBytes()).parseClaimsJws(jwtToken).getBody();
	}
	
	private void setUpSpringAuthentication(Claims claims) {
		List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList((String)claims.get("authorities"));
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
		SecurityContextHolder.getContext().setAuthentication(auth);
	}
	
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			if (checkToken(request, response)) {
				Claims claims = validateToken(request);
				if (claims.get(JwtTokenUtils.AUTHORITIES_KEY) != null) {
					setUpSpringAuthentication(claims);
				} else {
					SecurityContextHolder.clearContext();
				}
			}
			chain.doFilter(request, response);
		} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
			return;
		}
    }
}
