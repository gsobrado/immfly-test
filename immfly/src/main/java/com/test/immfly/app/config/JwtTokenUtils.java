package com.test.immfly.app.config;

import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtils {
	
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "authorities";

	@Value("${jwt.key}")
	String  signingKey;
    
	public String generateToken(Authentication authentication) {

		  final String authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","));

		  String token = Jwts.builder()
	                .setSubject(authentication.getName())
	                .claim(AUTHORITIES_KEY, authorities)
	                .signWith(SignatureAlgorithm.HS512, signingKey.getBytes())
	                .setIssuedAt(new Date(System.currentTimeMillis()))
	                .setExpiration(getEndOfTheDay())
	                .compact();
		  return token;
	}

	private Date getEndOfTheDay() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		
		return new Date (cal.getTimeInMillis());
	}
	
	public String getSigningKey() {
		return signingKey;
	}
}
