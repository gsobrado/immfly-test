package com.test.immfly.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.test.immfly.app.model.Fligth;

@Configuration
public class RedisConfiguration {

	@Value("${redis.host}")
	String  hostname;
	
	@Bean
	JedisConnectionFactory redisConnectionFactory() {
		RedisStandaloneConfiguration redisConf = new RedisStandaloneConfiguration();
		redisConf.setHostName(hostname);
	    return new JedisConnectionFactory(redisConf);
	}
	
	@Bean
	RedisTemplate<String, Fligth> redisTemplateFligth() {
		final RedisTemplate<String, Fligth> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(redisConnectionFactory());
		return redisTemplate;
	}
	
}
