package com.test.immfly.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.test.immfly.app.config.JwtTokenUtils;
import com.test.immfly.app.model.User;

@RestController
@RequestMapping("/immfly")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    @PostMapping(value = "/login")
    @CrossOrigin
    public ResponseEntity<String> login(@RequestBody User usuario) throws AuthenticationException, Exception {

		Authentication authentication = null;

    	try {
			UsernamePasswordAuthenticationToken userToAuthenticate = new UsernamePasswordAuthenticationToken(usuario.getUsername(), usuario.getPassword());
			authentication = authenticationManager.authenticate(userToAuthenticate);
	        SecurityContextHolder.getContext().setAuthentication(authentication);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		}
    	
    	final String token = jwtTokenUtils.generateToken(authentication);
    	
        return ResponseEntity.ok(token);
    }
    
}
