package com.test.immfly.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.test.immfly.app.model.Fligth;
import com.test.immfly.app.service.FligthService;

@RestController
@RequestMapping("/immfly")
@CrossOrigin
public class FligthController {
	
	@Autowired
	private FligthService fligthService;
	
	@GetMapping("/filterFligths/{tailNumber}/{fligthNumber}")
	public ResponseEntity<Fligth> filgetFligths(@PathVariable(name = "tailNumber") String tailNumber, @PathVariable(name = "fligthNumber") Long fligthNumber){
		try {
			Fligth response = fligthService.getFligth(tailNumber, fligthNumber);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			System.out.println(e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "General Error filtering the Flingths");
		}
	}
	
}
