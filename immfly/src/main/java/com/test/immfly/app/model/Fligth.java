package com.test.immfly.app.model;

import java.io.Serializable;

public class Fligth implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String ident;
	private String faFlightID;
	private String airline;
	private String airline_iata;
	private Long flightnumber;
	private String tailnumber;
	private String type;
	private String codeshares;
	private Airport origin;
	private Airport destination;
	private Boolean blocked;
	private Boolean diverted;
	private Boolean cancelled;
	
	public Fligth(String ident, String faFlightID, String airline, 
			String airline_iata, Long flightnumber, String tailnumber, 
			String type, String codeshares, Airport origin, Airport destination,
			Boolean blocked, Boolean diverted, Boolean cancelled) {
		this.ident = ident;
		this.faFlightID = faFlightID;
		this.airline = airline;
		this.airline_iata = airline_iata;
		this.flightnumber = flightnumber;
		this.tailnumber = tailnumber;
		this.type = type;
		this.codeshares = codeshares;
		this.origin = origin;
		this.destination = destination;
		this.blocked = blocked;
		this.diverted = diverted;
		this.cancelled = cancelled;
	}

	public String getIdent() {
		return ident;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	public String getFaFlightID() {
		return faFlightID;
	}

	public void setFaFlightID(String faFlightID) {
		this.faFlightID = faFlightID;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getAirline_iata() {
		return airline_iata;
	}

	public void setAirline_iata(String airline_iata) {
		this.airline_iata = airline_iata;
	}

	public Long getFlightnumber() {
		return flightnumber;
	}

	public void setFlightnumber(Long flightnumber) {
		this.flightnumber = flightnumber;
	}

	public String getTailnumber() {
		return tailnumber;
	}

	public void setTailnumber(String tailnumber) {
		this.tailnumber = tailnumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCodeshares() {
		return codeshares;
	}

	public void setCodeshares(String codeshares) {
		this.codeshares = codeshares;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public Boolean getDiverted() {
		return diverted;
	}

	public void setDiverted(Boolean diverted) {
		this.diverted = diverted;
	}

	public Boolean getCancelled() {
		return cancelled;
	}

	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	public Airport getOrigin() {
		return origin;
	}

	public void setOrigin(Airport origin) {
		this.origin = origin;
	}

	public Airport getDestination() {
		return destination;
	}

	public void setDestination(Airport destination) {
		this.destination = destination;
	}

	@Override
    public String toString() { 
        return String.format(ident); 
    } 
	
}
