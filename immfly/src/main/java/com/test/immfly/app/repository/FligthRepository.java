package com.test.immfly.app.repository;

import java.util.List;

import com.test.immfly.app.model.Fligth;

public interface FligthRepository {

	List<Fligth> fildAll();
	
	void save(Fligth fligth);
}
