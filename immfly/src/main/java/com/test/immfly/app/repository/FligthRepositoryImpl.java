package com.test.immfly.app.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.test.immfly.app.model.Fligth;

@Repository
public class FligthRepositoryImpl implements FligthRepository{
	
	private static final String KEY = "Fligth";

	private RedisTemplate<String, Fligth> redisTemplate;
	private HashOperations hashOperations;
	
	public FligthRepositoryImpl(RedisTemplate<String, Fligth> redisTemplate) {
		this.redisTemplate = redisTemplate;
		hashOperations = redisTemplate.opsForHash();
	}
	
	@Override
	public List<Fligth> fildAll() {
		return hashOperations.values(KEY);
	}

	@Override
	public void save(Fligth fligth) {
		hashOperations.put(KEY, fligth.getIdent(), fligth);
	}
		
}
