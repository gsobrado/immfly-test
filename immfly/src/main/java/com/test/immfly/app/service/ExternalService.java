package com.test.immfly.app.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.test.immfly.app.model.Fligth;
import com.test.immfly.app.repository.FligthRepository;

@Service
public class ExternalService {
	
	private static final Logger logger = Logger.getLogger(ExternalService.class);
	
	@Value("${json.webserver.url}")
	String  url;

	@Autowired
	private FligthRepository fligthRepository;
	
	@Autowired
	private RestTemplate restTempalte;

	public List<Fligth> getExternasFligths(String tail) {
		logger.info("Call json-web-server with tailNumber: " + tail);
		Fligth[] claimResponse = restTempalte.getForObject(url + "?tailnumber=" + tail, Fligth[].class);
		List<Fligth> resp = Arrays.stream(claimResponse).collect(Collectors.toList());
		if(resp != null) {
			resp.stream().forEach(fligth -> fligthRepository.save(fligth));			
		}
		logger.info("Response json-web-server with " + claimResponse.length + " Fligths");
		return resp;
	}
	
}
