package com.test.immfly.app.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.immfly.app.model.Fligth;
import com.test.immfly.app.repository.FligthRepository;

@Service
public class FligthService {
	
	private static final Logger logger = Logger.getLogger(FligthService.class);
	
	@Autowired
	private ExternalService externalService; 
	
	@Autowired
	private FligthRepository fligthRepository;
	
	public Fligth getFligth(String tail, Long fligthN){
		logger.info("Call getFligth with tailNumber: " + tail + " and fligthNumber: " + fligthN);
		Fligth response = filterFligths(tail, fligthN, fligthRepository.fildAll());
		if(response == null) {
			response = filterFligths(tail, fligthN, externalService.getExternasFligths(tail));
		}
		logger.info("Response getFligth " + response.toString());
		return response;
	}
	
	private Fligth filterFligths(String tail, Long fligthN, List<Fligth> allFligths) {
		if(allFligths != null) {
			return allFligths.stream().filter(fligth -> fligth.getTailnumber().equals(tail) && fligth.getFlightnumber().equals(fligthN)).findAny().orElse(null);			
		}
		return null;
	}
	
}
