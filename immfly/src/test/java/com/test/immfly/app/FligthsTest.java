package com.test.immfly.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.test.immfly.app.mocks.WebServiceFligthsMock;
import com.test.immfly.app.model.Fligth;
import com.test.immfly.app.repository.FligthRepository;
import com.test.immfly.app.service.ExternalService;
import com.test.immfly.app.service.FligthService;

@SpringBootTest
class FligthsTest {
	
	private static final Logger logger = Logger.getLogger(FligthsTest.class);
	
	@Mock
	ExternalService externalService;
	
	@Mock
	FligthRepository fligthRepository;

	@InjectMocks
	FligthService fligthService;
	
	
	@BeforeEach
	public void before() {
		given(fligthRepository.fildAll()).willReturn(new ArrayList<Fligth>()).willReturn(WebServiceFligthsMock.getFligths());
		given(externalService.getExternasFligths(anyString())).willReturn(WebServiceFligthsMock.getFligths());
	}
	
	@Test
	void fillterTest() {
		logger.info("Inicia Mokito Test");
		Fligth filtereds = fligthService.getFligth("EC-MYT", 654L);
		assertThat(filtereds).isNotNull();
		assertThat(filtereds.getIdent()).isEqualTo("IBB690");
		then(fligthRepository).should(times(1)).fildAll();
		then(externalService).should(times(1)).getExternasFligths(anyString());
	}
	
	@Test
	void fillterTest2() {
		logger.info("Inicia Mokito Test 2");
		Fligth filtereds = fligthService.getFligth("EC-MYT", 654L);
		
		then(fligthRepository).should(times(1)).fildAll();
		then(externalService).should(times(1)).getExternasFligths(anyString());
		
		filtereds = fligthService.getFligth("EC-MYT", 655L);
		
		assertThat(filtereds).isNotNull();
		assertThat(filtereds.getIdent()).isEqualTo("IBB654");
		then(fligthRepository).should(times(2)).fildAll();
		then(externalService).should(times(1)).getExternasFligths(anyString());
	}
	
}
