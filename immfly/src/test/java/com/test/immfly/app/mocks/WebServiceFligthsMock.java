package com.test.immfly.app.mocks;

import java.util.ArrayList;
import java.util.List;

import com.test.immfly.app.model.Airport;
import com.test.immfly.app.model.Fligth;

public class WebServiceFligthsMock {
	
	public static List<Fligth> getFligths(){
		
		List<Fligth> fligths = new ArrayList<Fligth>();
		
		Airport airpor1 = new Airport("GCXO", "Tenerife"			, "TFN", "Tenerife North (Los Rodeos)");
		Airport airpor2 = new Airport("GCGM", "La Gomera"			, "GMZ", "La Gomera");
		Airport airpor3 = new Airport("EDDB", "	Schönefeld"			, "SXF", "Aeropuerto de Berlín-Schönefeld");
		Airport airpor4 = new Airport("EDDT", "Berlín"				, "TXL", "Aeropuerto de Berlín-Tegel");
		Airport airpor5 = new Airport("EDDW", "Bremen"				, "BRE", "Aeropuerto de Bremen");
		Airport airpor6 = new Airport("EDDK", "Colonia"             , "CGN", "Aeropuerto de Colonia/Bonn");
		Airport airpor7 = new Airport("EDLW", "Dortmund"            , "DTM", "Aeropuerto de Dortmund");
		Airport airpor8 = new Airport("EDDC", "Dresde"              , "DRS", "Aeropuerto de Dresde");
		Airport airpor9 = new Airport("EDDL", "Düsseldorf"          , "DUS", "Aeropuerto de Düsseldorf");
		Airport airpor10 = new Airport("EDDE", "Erfurt"             , "ERF", "Aeropuerto de Erfurt-Weimar");
		Airport airpor11 = new Airport("EDDF", "Fráncfort del Meno" , "FRA", "Aeropuerto de Fráncfort del Meno");
		Airport airpor12 = new Airport("EDFH", "Lautzenhausen"      , "HHN", "Aeropuerto de Fráncfort-Hahn");
		Airport airpor13 = new Airport("EDNY", "Friedrichshafen"    , "FDH", "Aeropuerto de Friedrichshafen");
		
		fligths.add(new Fligth("IBB690", "IBB653-1581399936-airline-0136", "IBB", "NT", 654L, "EC-MYT", "Form_Airline","IBE195", airpor1, airpor2, true, true, true));
		fligths.add(new Fligth("IBB654", "IBB654-1581399936-airline-0136", "IBB", "NT", 655L, "EC-MYT", "Form_Airline","IBE124", airpor4, airpor12, false, true, false));
		fligths.add(new Fligth("IBB655", "IBB655-1581399936-airline-0136", "IBB", "NT", 656L, "EC-MYT", "Form_Airline","IBE125", airpor5, airpor3, false, false, true));
		fligths.add(new Fligth("IBB656", "IBB656-1581399936-airline-0136", "IBB", "NT", 657L, "EC-MYP", "Form_Airline","ITA112", airpor7, airpor8, false, false, false));
		fligths.add(new Fligth("IBB657", "IBB657-1581399936-airline-0136", "IBB", "NT", 658L, "EC-MYP", "Form_Airline","ITA123", airpor9, airpor4, true, true, false));
		fligths.add(new Fligth("IBB658", "IBB658-1581399936-airline-0136", "IBB", "NT", 659L, "EC-MYK", "Form_Airline","IBE133", airpor10, airpor7, false, true, false));
		fligths.add(new Fligth("IBB659", "IBB659-1581399936-airline-0136", "IBB", "NT", 6544L, "EC-MYK", "Form_Airline","IBE166", airpor11, airpor5, false, true, false));
		fligths.add(new Fligth("IBB660", "IBB660-1581399936-airline-0136", "IBB", "NT", 644L, "EC-MYT", "Form_Airline","IBE167", airpor6, airpor13, false, false, true));
		
		return fligths;
	}

}
